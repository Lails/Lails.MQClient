﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lails.MQClient
{
    public class MessageData
    {
        public byte[] Data { get; set; }
        public string Topic { get; set; }
        public string Key { get; set; }
    }
}
