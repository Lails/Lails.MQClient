﻿using System;
using System.Text;
using Lails.MQClient.Base.NetCore;
using Lails.MQClient.RabbitMQ.NetCore;

namespace Lails.MQClient.NetCore.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Subscribe();
            Publish();

            Console.Read();
        }

        private static void Subscribe()
        {
            //string consumername1 = "consumer1", consumername2 = "consumer1";    //独立订阅
            string consumername1 = "consumer1", consumername2 = "consumer2";    //共同订阅
            Lails.MQClient.Base.NetCore.MQClient consumer1 = CreateClient(consumername1);
            Lails.MQClient.Base.NetCore.MQClient consumer2 = CreateClient(consumername2);
            consumer1.Connect();
            consumer1.Subscribe("msg.*", msg =>
            {
                Console.WriteLine($"订阅1接收消息：Topic={msg.Topic}, Data={Encoding.UTF8.GetString(msg.Data)}");
                consumer1.UnSubscribe("msg");
                Console.WriteLine("退订consumer1");
                return true; //ACK
            });
            consumer2.Connect();
            consumer2.Subscribe("msg", msg =>
            {
                Console.WriteLine($"订阅2接收消息：Topic={msg.Topic}, Data={Encoding.UTF8.GetString(msg.Data)}");
                consumer1.UnSubscribe("msg");
                Console.WriteLine("退订consumer1");
                return true; //ACK
            });
        }

        private static void Publish()
        {
            Lails.MQClient.Base.NetCore.MQClient publisher = CreateClient("publisher");
            publisher.Connect();
            string message = $"Rabbitmq Publish Message {DateTime.Now.ToString()}";
            byte[] body1 = Encoding.UTF8.GetBytes(message + "_01");
            byte[] body2 = Encoding.UTF8.GetBytes(message + "_02");
            publisher.Publish("msg.a", body1);
            publisher.Publish("msg.b", body2);
            Console.WriteLine($"发送消息：{message}");
        }

        private static Lails.MQClient.Base.NetCore.MQClient CreateClient(string clientID)
        {
            //MQClient client = new RabbitMQClient("mq.lails.cc", "demo", "demo", clientID, appid: "demo");
            Lails.MQClient.Base.NetCore.MQClient client = new RabbitMQClient("127.0.0.1:18000", "jtttest", "jtttest", null);
            return client;
        }
    }
}
