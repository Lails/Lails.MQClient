﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lails.MQClient.Base.Standard
{
    public abstract class MQClient
    {
        public string ClientID { get; protected set; }
        public string Adress { get; protected set; }
        public string Username { get; protected set; }
        public string Password { get; protected set; }
        public bool Connected { get; protected set; }

        public MQClient(string address, string username, string password, string clientID)
        {
            this.Adress = address;
            this.Username = username;
            this.Password = password;
            this.ClientID = clientID;
        }

        public void Connect()
        {
            if (!Connected)
            {
                if(_Connect()) Connected = true;
            }
        }

        public void DisConnect()
        {
            if (Connected)
            {
                if(_DisConnect()) Connected = false;
            }
        }

        protected abstract bool _Connect();

        protected abstract bool _DisConnect();

        public abstract bool Publish(string topic, byte[] body, MessageArgs arg = null);

        public abstract bool Subscribe(string topic, Func<byte[], bool> subscribe, MessageArgs arg = null);

        public abstract bool Subscribe(string topic, Func<MessageData, bool> subscribe, MessageArgs arg = null);

        public abstract bool UnSubscribe(string topic);

        protected abstract string FormatTopic(string topic);
    }
}
