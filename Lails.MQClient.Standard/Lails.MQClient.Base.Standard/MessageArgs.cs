﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lails.MQClient.Base.Standard
{
    public abstract class MessageArgs
    {
        /// <summary>
        /// 是否持久化   
        /// </summary>
        public bool Durable { get; set; }
    }
}
