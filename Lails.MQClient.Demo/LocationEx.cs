﻿//using Cheyilian.Protocol.JT808;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lails.MQClient.Demo
{
    [ProtoContract]
    public class LocationEx
    {
        [ProtoMember(1)]
        public double Lon;//经度
        [ProtoMember(2)]
        public double Lat;//纬度
        [ProtoMember(3)]
        public int Speed;//GPS速度，单位：km/h
        [ProtoMember(4)]
        public float Direction;//方向，正北方向为0度，顺时针递增
        [ProtoMember(5)]
        public int Altitude;//海拔高度，单位为米(m) 
        [ProtoMember(6)]
        public DateTime LocateTime;//定位时间
        [ProtoMember(7)]
        public long State;//状态，具体定义见JT808协议中位置信息汇报(消息ID：0x0200)
        [ProtoMember(8)]
        public long Alarm;//报警，具体定义见JT808协议中位置信息汇报(消息ID：0x0200)
        [ProtoMember(9)]
        public float Mileage;//里程，单位：km
        [ProtoMember(10)]
        public float OilOhm;//油量，单位升
        [ProtoMember(11)]
        public int SenseSpeed;//行驶记录功能获取的速度，单位：km/h
        [ProtoMember(12)]
        public uint ExVhlSignalState;//扩展车辆信号状态位,属于位置信息汇报附加项
        [ProtoMember(13)]
        public int WLSignal;//无线通信网络信号强度
        [ProtoMember(14)]
        public int GNSSCount;//GNSS定位卫星个数
        [ProtoMember(15)]
        public DateTime ReceiveTime;//数据接收时间
        [ProtoMember(16)]
        public string Key;//数据提交Key标识，即设备唯一标识
        [ProtoMember(17)]
        public string Permutation;//数据解析器名称
        [ProtoMember(18)]
        public string EndPointName;//远程地址
    }
}
