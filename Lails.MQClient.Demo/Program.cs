﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lails.MQClient.Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Subscribe();
            Publish();

            Console.Read();
        }

        private static void Subscribe()
        {
            //string consumername1 = "consumer1", consumername2 = "consumer1";    //独立订阅
            string consumername1 = "consumer1", consumername2 = "consumer2";    //共同订阅
            MQClient consumer1 = CreateClient(consumername1);
            MQClient consumer2 = CreateClient(consumername2);
            consumer1.Connect();
            consumer1.Subscribe("msg.*", msg =>
            {
                Console.WriteLine($"订阅1接收消息：Topic={msg.Topic}, Data={Encoding.UTF8.GetString(msg.Data)}");
                consumer1.UnSubscribe("msg");
                Console.WriteLine("退订consumer1");
                return true; //ACK
            });
            consumer2.Connect();
            consumer2.Subscribe("msg", msg =>
            {
                Console.WriteLine($"订阅2接收消息：Topic={msg.Topic}, Data={Encoding.UTF8.GetString(msg.Data)}");
                consumer1.UnSubscribe("msg");
                Console.WriteLine("退订consumer1");
                return true; //ACK
            });
        }

        private static void Publish()
        {
            MQClient publisher = CreateClient("publisher");
            publisher.Connect();
            string message = $"Rabbitmq Publish Message {DateTime.Now.ToString()}";
            byte[] body1 = Encoding.UTF8.GetBytes(message + "_01");
            byte[] body2 = Encoding.UTF8.GetBytes(message + "_02");
            publisher.Publish("msg.a", body1);
            publisher.Publish("msg.b", body2);
            Console.WriteLine($"发送消息：{message}");
        }

        private static MQClient CreateClient(string clientID)
        {
            //MQClient client = new RabbitMQClient("mq.lails.cc", "demo", "demo", clientID, appid: "demo");
            MQClient client = new RabbitMQClient("114.115.141.103:5672", "yilink", "iThings2018", null);
            return client;
        }
    }
}
