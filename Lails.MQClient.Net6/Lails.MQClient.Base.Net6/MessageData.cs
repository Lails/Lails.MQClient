﻿namespace Lails.MQClient.Base.Net6
{
    /// <summary>
    /// 
    /// </summary>
    public class MessageData
    {
        /// <summary>
        /// 
        /// </summary>
        public byte[] Data { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Topic { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Key { get; set; }
    }
}