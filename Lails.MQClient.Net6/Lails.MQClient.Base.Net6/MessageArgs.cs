﻿namespace Lails.MQClient.Base.Net6
{
    public abstract class MessageArgs
    {
        /// <summary>
        /// 是否持久化   
        /// </summary>
        public bool Durable { get; set; }
    }
}
